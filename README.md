# MTh DAT20 Rekonstruktion Lagerbereich 3D Modell

This repository holds only information which are open to share. All content which is company related doesn't appear on in repository.
So there are some files which are not fully reproduceable due to missing constraints, for ex. production data.

### Blender Model
There are two different blender models. One just generate the test example which is used in the master thesis and it is called ```test```. 
the other is generating a warehouse area, which can be configured in number of aisles and shelfs directly in the python code.

### master-thesis
for the reason the master-thesis is not open for all, this folder contains only the bib file and the online resources.

### sklearn
the modified sklearn mds method is present in this folder.

### warehouse-reconstruction
This folder only contains jupyter-notebooks which doesn't contains company related stuff.
It shows only the results of the master-thesis and some preperation steps for the simulation or test data.
