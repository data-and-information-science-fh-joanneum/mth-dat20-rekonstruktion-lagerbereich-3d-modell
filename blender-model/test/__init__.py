# context.area: VIEW_3D
bl_info = {
    "name": "test",
    "author": "Alexander Schadler",
    "version": (1, 0),
    "blender": (2, 80, 0),
    "location": "View3D > Add > Mesh > New Object",
    "description": "test",
    "warning": "",
    "doc_url": "",
    "category": "Add Mesh",
}

import bpy
from bpy.types import Operator
from bpy.props import FloatVectorProperty
from bpy_extras.object_utils import AddObjectHelper, object_data_add
from mathutils import Vector
import math
import string
import random
import re
import numpy as np
import pandas as pd
import pyarrow


"""
Function to create a object
"""
def create_cube(self, context, object_name):
    verts = [
        # cube
        Vector((0, 0, 0)), #LVU 0
        Vector((0, 4, 0)), #LHU 1
        Vector((2, 4, 0)), #RHU 2
        Vector((2, 0, 0)), #RVU 3
        Vector((0, 0, 2)), #LVO 4
        Vector((0, 4, 2)), #LHO 5
        Vector((2, 4, 2)), #RHO 6
        Vector((2, 0, 2)), #RVO 7
    ]

    edges = []
    faces = [[0, 1, 2, 3], [4, 5, 6, 7], [4, 7, 3, 0], [5, 6, 2, 1], [4, 5, 1, 0],[7, 6, 2, 3]]
    
    mesh = bpy.data.meshes.new(name=object_name)
    mesh.from_pydata(verts, edges, faces)
    # useful for development when the mesh may be invalid.
    # mesh.validate(verbose=True)
    object_data_add(context, mesh, operator=self)


def create_object(self, context, object_name):
    verts = [
        # cube
        Vector((0, 0, 0)), #LVU 0
        Vector((0, 4, 0)), #LHU 1
        Vector((2, 4, 0)), #RHU 2
        Vector((2, 0, 0)), #RVU 3
        Vector((0, 0, 1)), #LVM 4
        Vector((0, 4, 1)), #LHM 5
        Vector((2, 4, 1)), #RHM 6
        Vector((2, 0, 1)), #RVM 7
        Vector((0, 1, 2)), #LMO 8
        Vector((2, 1, 2)), #RMO 9
        Vector((0, 3, 2)), #LM2O 10
        Vector((2, 3, 2)), #RM2O 11
        Vector((0, 1, 1)), #LMM 12
        Vector((2, 1, 1)), #RMM 13
        Vector((0, 3, 1)), #LM2M 14
        Vector((2, 3, 1)), #RM2M 15
    ]

    edges = []
    #faces = [[0, 1, 2, 3], [4, 5, 6, 7], [4, 7, 3, 0], [5, 6, 2, 1], [4, 5, 1, 0],[7, 6, 2, 3]]
    faces = [[0, 1, 2, 3], [4, 7, 3, 0], [5, 6, 2, 1], [4, 5, 1, 0], [7, 6, 2, 3], [8,9,11,10], [12,8,9,13], [14,10,11,15] ]
    
    mesh = bpy.data.meshes.new(name=object_name)
    mesh.from_pydata(verts, edges, faces)
    # useful for development when the mesh may be invalid.
    # mesh.validate(verbose=True)
    object_data_add(context, mesh, operator=self)

"""
function to extract the location_name from the object
to get the level of the location we have to substract 31 from the index as the indeces for the locations of the shelf are at the position 32 to 35.
"""
def get_location_name(location_object):
    return location_object.id_data.name.rsplit('.', 1)[0] + "." + str(location_object.index)


def is_duplicate(location, locations):
    for loc in locations:
        if loc.id_data.name == location.id_data.name and loc.index == location.index:
            return True
    return False

def get_random_location():
    obj = random.choice([ob for ob in bpy.data.objects if re.search("Podium", ob.name)])
    return random.choice(obj.data.vertices[0:16]) # only these incides holds the locations

"""
Function to generate data from simulated picks
number_of_picks: generate data from the given amount of picks
draw_routes: draw also the route of the picks in the warehouse area
general info: index 36 represent the pick point, which is the location from the picker, the palce where as the picker tries to pick the item from
"""
def create_dataset(self, context, number_of_picks, generate_output_file=True):
    ## 
    # initiate empty array for pandas dataset
    loc_distances = np.empty((0,4), dtype='object') #  ## order_id, location_from, location_to, distance
    
    for pick_count in range(number_of_picks):
        locations = []
        #random.seed(30+pick_count) # used for generating the data
        for loc in range(random.randint(1,15)): # shuffle between 1 and 8 locations 
            location = get_random_location()
            
            while is_duplicate(location, locations):
                location = get_random_location()

            locations.append(location)

        
        shape_size = len(locations)
            
        loc_distance = np.empty((shape_size +1,4), dtype='object') ## order_id, location_from, location_to, distance
        
        last_wp = None
        last_location = locations[0]
        sl_idx = 0
        distance = 0
        for wp in locations:
            if last_wp is not None:
                distance += math.dist(last_wp.co, wp.co)
            
            loc_distance[sl_idx] = [pick_count, get_location_name(last_location), get_location_name(wp), distance]
            sl_idx += 1
            distance = 0
            last_location = wp                
            last_wp = wp
            
        loc_distances = np.vstack((loc_distances, loc_distance))
    df = pd.DataFrame(loc_distances, columns = ['id','source_location','target_location', 'distance'] )
    self.report({'INFO'}, str(df))
    if generate_output_file:
        df.to_feather("gen_data/simulated_testdata.feather")
        df.to_csv("gen_data/simulated_testdata.csv")
            

class OBJECT_OT_add_object(Operator, AddObjectHelper):
    """Create a new Mesh Object"""
    bl_idname = "mesh.add_object"
    bl_label = "Add Warehouse area"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        # define GLOBAL variables which won't be changed after creating the warehouse

        create_object(self, context, 'Podium')   
        create_dataset(self,context, number_of_picks=1000, generate_output_file=True)
        return {'FINISHED'}


# Registration

def add_object_button(self, context):
    self.layout.operator(
        OBJECT_OT_add_object.bl_idname,
        text="test_object",
        icon='PLUGIN')

def register():
    bpy.utils.register_class(OBJECT_OT_add_object)
    bpy.types.VIEW3D_MT_mesh_add.append(add_object_button)
    
def unregister():
    bpy.utils.unregister_class(OBJECT_OT_add_object)
    bpy.types.VIEW3D_MT_mesh_add.remove(add_object_button)
    

if __name__ == "__main__":
    register()
