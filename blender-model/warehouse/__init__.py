# context.area: VIEW_3D
bl_info = {
    "name": "Warehouse area",
    "author": "Alexander Schadler",
    "version": (1, 0),
    "blender": (2, 80, 0),
    "location": "View3D > Add > Mesh > New Object",
    "description": "Create a warehouse area",
    "warning": "",
    "doc_url": "",
    "category": "Add Mesh",
}

import bpy
from bpy.types import Operator
from bpy.props import FloatVectorProperty
from bpy_extras.object_utils import AddObjectHelper, object_data_add
from mathutils import Vector
import math
import string
import random
import re
import numpy as np
import pandas as pd
import pyarrow


def add_visted_location(self, context, coordinates, location_name):
    
    cube_size = 0.1

    x = coordinates[0] - cube_size/2
    y = coordinates[1] - cube_size/2
    z = coordinates[2]

    verts = [
        Vector((x, y, z)), 
        Vector((x, y+cube_size, z)),
        Vector((x+cube_size, y+cube_size, z)),
        Vector((x+cube_size, y, z)),   
        Vector((x, y, z + cube_size)), 
        Vector((x, y+cube_size, z + cube_size)),
        Vector((x+cube_size, y+cube_size, z + cube_size)),
        Vector((x+cube_size, y,z + cube_size)),
        #location
        Vector((x+cube_size/2,y+cube_size/2, z + cube_size)), #7
    ]

    edges = []
    faces = [[0, 1, 2, 3], [4, 5, 6, 7], [4, 7, 3, 0], [5, 6, 2, 1], [4, 5, 1, 0],[7, 6, 2, 3]]

    mesh = bpy.data.meshes.new(name=location_name)

    orange = bpy.data.materials.new("orange")
    orange.diffuse_color = ( 1.0, 0.36, 0.0, 1)
    mesh.materials.append(orange)

    mesh.from_pydata(verts, edges, faces)
    # useful for development when the mesh may be invalid.
    # mesh.validate(verbose=True)
    object_data_add(context, mesh, operator=self)

"""
Function to create the waypoints between the shelfs
"""
def add_way_point(self, context, coordinates, waypoint_name):
    
    cube_size = 0.05

    x = coordinates[0] - cube_size/2
    y = coordinates[1] - cube_size/2
    z = coordinates[2] - cube_size

    verts = [
        Vector((x, y, z)), 
        Vector((x, y+cube_size, z)),
        Vector((x+cube_size, y+cube_size, z)),
        Vector((x+cube_size, y, z)),   
        Vector((x, y, z + cube_size)), 
        Vector((x, y+cube_size, z + cube_size)),
        Vector((x+cube_size, y+cube_size, z + cube_size)),
        Vector((x+cube_size, y,z + cube_size)),
        #location
        Vector((x+cube_size/2,y+cube_size/2, z + cube_size)), #7
    ]

    edges = []
    faces = [[0, 1, 2, 3], [4, 5, 6, 7], [4, 7, 3, 0], [5, 6, 2, 1], [4, 5, 1, 0],[7, 6, 2, 3]]

    mesh = bpy.data.meshes.new(name=waypoint_name)
    mesh.from_pydata(verts, edges, faces)
    # useful for development when the mesh may be invalid.
    # mesh.validate(verbose=True)
    object_data_add(context, mesh, operator=self)

    
    
"""
Function to create the point for the base
"""
def add_base(self, context, coordinates):
    cube_size = 0.1
    x = coordinates[0] - cube_size/2
    y = coordinates[1]
    z = coordinates[2] - cube_size

    verts = [
        Vector((x, y, z)), 
        Vector((x, y+cube_size, z)),
        Vector((x+cube_size, y+cube_size, z)),
        Vector((x+cube_size, y, z)),   
        Vector((x, y, z + cube_size)), 
        Vector((x, y+cube_size, z + cube_size)),
        Vector((x+cube_size, y+cube_size, z + cube_size)),
        Vector((x+cube_size, y, z + cube_size)),
        #location
        Vector((x+cube_size/2,y+cube_size/2, z + cube_size)), #7
    ]

    edges = []
    faces = [[0, 1, 2, 3], [4, 5, 6, 7], [4, 7, 3, 0], [5, 6, 2, 1], [4, 5, 1, 0],[7, 6, 2, 3]]

    mesh = bpy.data.meshes.new(name="Base")
    mesh.from_pydata(verts, edges, faces)
    # useful for development when the mesh may be invalid.
    # mesh.validate(verbose=True)
    object_data_add(context, mesh, operator=self)


"""
Function to create a shelf
"""
def add_shelf(self, context, coordinates, shelf_deep, shelf_name, aisle_width, pick_heigth, orientation="l"):
    """
        1 unit = 1000mm
        typical dimensions of a shelf = 1800x900x400mm
    """
    shelf_height = 1.8 # _> 1800mm
    shelf_width = 0.9 # -> 900mm

    shelf_height_level_4 = shelf_height*0.775
    shelf_height_level_3 = shelf_height*0.55
    shelf_height_level_2 = shelf_height*0.325
    shelf_height_level_1 = shelf_height*0.1

    x = coordinates[0]
    y = coordinates[1]

    x_loc_pos = x + shelf_deep/2
    if orientation == "l":
        x_pick_pos = x + shelf_deep  + aisle_width/2
    else:
        x_pick_pos = x - aisle_width/2

    def calculate_x_pick_point(b):
        pick_deep = 0.4 # the best picking deep to grap a item in a shelf is 400 as also referenced in (Martin2016)
        a = pick_deep
        c = np.sqrt(a**2 + b**2)
        
        if orientation == "l":
            m = 1
        else:
            m = -1
        
        return (a - (c - a))*m

    verts = [
        # top level
        Vector((x, y, shelf_height)), 
        Vector((x, y + shelf_width, shelf_height)),
        Vector((x + shelf_deep, y + shelf_width, shelf_height)),
        Vector((x + shelf_deep, y, shelf_height)),
        # 4th level
        Vector((x, y, shelf_height_level_4)), 
        Vector((x, y + shelf_width, shelf_height_level_4)),
        Vector((x + shelf_deep, y + shelf_width, shelf_height_level_4)),
        Vector((x + shelf_deep, y, shelf_height_level_4)),
        # 3rd level  
        Vector((x, y, shelf_height_level_3)),
        Vector((x, y +shelf_width, shelf_height_level_3)),
        Vector((x + shelf_deep, y + shelf_width, shelf_height_level_3)),
        Vector((x + shelf_deep, y, shelf_height_level_3)),
        # 2nd level
        Vector((x, y, shelf_height_level_2)),
        Vector((x, y + shelf_width, shelf_height_level_2)),
        Vector((x + shelf_deep, y + shelf_width, shelf_height_level_2)),
        Vector((x + shelf_deep, y, shelf_height_level_2)),
        # 1st level
        Vector((x, y, shelf_height_level_1)),
        Vector((x, y + shelf_width, shelf_height_level_1)),
        Vector((x + shelf_deep, y + shelf_width, shelf_height_level_1)),
        Vector((x + shelf_deep, y, shelf_height_level_1)),
        # bottom
        Vector((x, y, 0)),
        Vector((x, y + shelf_width, 0)),
        Vector((x + shelf_deep, y + shelf_width, 0)),
        Vector((x + shelf_deep, y, 0)),
        #Steher top
        Vector((x+0.01, y, shelf_height)), #24 
        Vector((x+0.01, y + shelf_width, shelf_height)), #25
        Vector((x-0.01 + shelf_deep, y + shelf_width, shelf_height)), #26
        Vector((x-0.01 + shelf_deep, y, shelf_height)), #27
        #Steher bottom
        Vector((x+0.01, y, 0)), #28 
        Vector((x+0.01, y + shelf_width, 0)), #29
        Vector((x-0.01 + shelf_deep, y + shelf_width, 0)), #30
        Vector((x-0.01 + shelf_deep, y, 0)), #31
        #locations
        #level 1
        Vector((x_loc_pos, y + shelf_width/2, shelf_height_level_1)), #32
        #level 2
        Vector((x_loc_pos, y + shelf_width/2, shelf_height_level_2)), #33
        #level 3
        Vector((x_loc_pos, y + shelf_width/2, shelf_height_level_3)), #34
        #level 4
        Vector((x_loc_pos, y + shelf_width/2, shelf_height_level_4)), #35
        # pick point level 1
        Vector((x_loc_pos + calculate_x_pick_point(np.abs(shelf_height_level_1 - pick_heigth)), y + shelf_width/2, pick_heigth)), #36
        # pick point level 2
        Vector((x_loc_pos + calculate_x_pick_point(np.abs(shelf_height_level_2 - pick_heigth)), y + shelf_width/2, pick_heigth)), #37
        # pick point level 3
        Vector((x_loc_pos + calculate_x_pick_point(np.abs(shelf_height_level_3 - pick_heigth)), y + shelf_width/2, pick_heigth)), #38
        # pick point level 4
        Vector((x_loc_pos + calculate_x_pick_point(np.abs(shelf_height_level_4 - pick_heigth)), y + shelf_width/2, pick_heigth)), #39

    ]

    edges = []
    faces = [[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, 10, 11], [12, 13, 14, 15], [16, 17, 18, 19],[0, 24, 28, 20],[1, 25, 29, 21],[3, 27, 31, 23],[2, 26, 30, 22]]
    
    mesh = bpy.data.meshes.new(name=shelf_name)
    mesh.from_pydata(verts, edges, faces)
    # useful for development when the mesh may be invalid.
    # mesh.validate(verbose=True)
    object_data_add(context, mesh, operator=self)


def extract_aisle_from_location(location):
    return location.id_data.name[6]

def extract_shelf_from_location(location):
    return int(location.id_data.name[7:10])

def extract_aisle_from_waypoint(waypoint):
    return waypoint.name[9]

def extract_shelf_from_waypoint(waypoint, is_vertex=False):
    if is_vertex:
        return int(waypoint.id_data.name[10:13])
    else:
        return int(waypoint.name[10:13])

def extract_shelf_from_waypoint_location(waypoint):
    return int(waypoint.id_data.name[10:13])

"""
Function which returns the closest and best waypoint according the snake algorithm for the given aisle and shelfnumber
the function needs to know if we going to the top or in direction to the bottom of the warehouse area.
"""
def find_best_waypoint_location(aisle, source_shelf_number, target_shelf_number, go_up, next_direction):    

    if not go_up:
        return [wp.data.vertices[7] for wp in WAYPOINTS if extract_aisle_from_waypoint(wp) == aisle and extract_shelf_from_waypoint(wp) <= (source_shelf_number if next_direction == 'desc' or target_shelf_number > source_shelf_number else target_shelf_number)].pop()
    else:
        return [wp.data.vertices[7] for wp in WAYPOINTS if extract_aisle_from_waypoint(wp) == aisle and (extract_shelf_from_waypoint(wp) > (source_shelf_number if next_direction == 'asc' or target_shelf_number < source_shelf_number else target_shelf_number))][0] 

"""
Function which returns the closest waypoint from the start point (shelf number = 1) of the given aisle 
"""
def get_closest_waypoint_location(aisle):
    return [wp.data.vertices[7] for wp in WAYPOINTS if extract_aisle_from_waypoint(wp) == aisle and extract_shelf_from_waypoint(wp) == 1][0]

"""
Function which returns the next waypoint from the last waypoint of the given aisle
"""
def get_successor_waypoint_location(aisle, prev_waypoint):
    return [wp.data.vertices[7] for wp in WAYPOINTS if extract_aisle_from_waypoint(wp) == aisle and extract_shelf_from_waypoint(wp) == extract_shelf_from_waypoint(prev_waypoint, is_vertex=True)][0]

"""
Function which returns the next waypoints between the shelfs.
As we cannot go from location to location directly, we have to pass certain waypoints to pass by the shelfs and "walk" on the aisles  
"""
def get_next_waypoints(source_location, target_location, last_direction, next_direction, return_to_bottom=False):
    
    waypoint_locations_to_visit = []
    if source_location.id_data.name.startswith("Base"):
        source_aisle = "BASE"
    else:
        source_aisle = source_location.id_data.name[6]
        source_shelf_number = int(source_location.id_data.name[7:10])
  
    if target_location.id_data.name.startswith("Base"):
        target_aisle = "BASE"
    else:
        target_aisle = target_location.id_data.name[6]
        target_shelf_number = int(target_location.id_data.name[7:10])
  
    
    if source_aisle == "BASE":
        waypoint_locations_to_visit.append(get_closest_waypoint_location(target_aisle))
    elif target_aisle == "BASE":
        waypoint_locations_to_visit.append(get_closest_waypoint_location(source_aisle))
    elif source_aisle != target_aisle:
        if return_to_bottom:
            waypoint_from_source = get_closest_waypoint_location(source_aisle)
            waypoint_locations_to_visit.append(waypoint_from_source)
        else:
            waypoint_from_source = find_best_waypoint_location(source_aisle, source_shelf_number, target_shelf_number, next_direction=next_direction, go_up=True if last_direction == "asc" else False)
            waypoint_locations_to_visit.append(waypoint_from_source)

        waypoint_to_target = get_successor_waypoint_location(target_aisle, waypoint_from_source)
        waypoint_locations_to_visit.append(waypoint_to_target)
                
    
    return waypoint_locations_to_visit

"""
Function which returns the pick point from the given location
"""
def get_pick_point_from_location(location):
    return location.id_data.vertices[location.index + 4] #pick locations have a offet of 4 

"""
Function which create the route/way between the warehouse shelfs according the given locations
The calculcation of the route is based on the snake algorithm
"""
def create_way(self, context, way_strategy, locations, draw_routes=False):

    def is_same_area(cur_location, next_location):
       cur_loc_wp = find_best_waypoint_location(extract_aisle_from_location(cur_location), extract_shelf_from_location(cur_location), extract_shelf_from_location(cur_location),True, 'asc')
       next_loc_wp = find_best_waypoint_location(extract_aisle_from_location(next_location), extract_shelf_from_location(next_location), extract_shelf_from_location(next_location),True, 'asc')
       return extract_shelf_from_waypoint(cur_loc_wp, is_vertex=True) == extract_shelf_from_waypoint(next_loc_wp, is_vertex=True)

    def resort_locations(locations, direction):
        if direction == 'asc':
            return sorted(locations, key=lambda e: (extract_aisle_from_location(e), extract_shelf_from_location(e), e.index)) # aisle asc, shelf_number asc, level asc
        else:
            return sorted(locations, key=lambda e: (extract_aisle_from_location(e), -extract_shelf_from_location(e), e.index)) # aisle asc, shelf_number desc, level asc

    def append_way(location, visited_location=None, last_direction=None, next_direction=None, switch_strategy=None):
        if visited_location is not None:
            wps = get_next_waypoints(visited_location, location, last_direction, next_direction, return_to_bottom = True if switch_strategy == 'return_adapted' else False)
            for wp in wps:
                way.append(wp)

        way.append(get_pick_point_from_location(location))
        way.append(location)
        #way.append(get_pick_point_from_location(location))
        visited_location = location
        locations.remove(location)
        return visited_location

    def change_direction(cur_location, next_location, last_direction):

        def get_wp_source(cur_location, next_location, cur_direction, next_direction):
            return find_best_waypoint_location(extract_aisle_from_location(cur_location), extract_shelf_from_location(cur_location), extract_shelf_from_location(next_location),True if cur_direction == "asc" else False, next_direction)
        
        def get_wp_target(next_location, wp_source):
            return get_successor_waypoint_location(extract_aisle_from_location(next_location), wp_source)

        wp_source_up =  get_wp_source(cur_location, next_location, 'asc', 'asc')
        wp_source_down = get_wp_source(cur_location, next_location, 'desc', 'desc')

        wp_target_up = get_wp_target(next_location, wp_source_up)
        wp_target_down = get_wp_target(next_location, wp_source_up)

        self.report({'INFO'}, "UP: wp_source (" + str(wp_source_up.id_data.name) + ") at (" + str(cur_location.id_data.name) + ") to (" + str(next_location.id_data.name) + ")")
        self.report({'INFO'}, "DOWN: wp_source (" + str(wp_source_down.id_data.name) + ") at (" + str(cur_location.id_data.name) + ") to (" + str(next_location.id_data.name) + ")")
        self.report({'INFO'}, "UP: wp_target (" + str(wp_target_up.id_data.name) + ") at (" + str(cur_location.id_data.name) + ") to (" + str(next_location.id_data.name) + ")")
        self.report({'INFO'}, "DOWN: wp_target (" + str(wp_target_down.id_data.name) + ") at  (" + str(cur_location.id_data.name) + ") to (" + str(next_location.id_data.name) + ")")

        up_shelf_count = abs(extract_shelf_from_waypoint(wp_source_up, is_vertex=True) - extract_shelf_from_location(cur_location)) \
                       + abs(extract_shelf_from_waypoint(wp_target_up, is_vertex=True) - extract_shelf_from_location(next_location))
        down_shelf_count = abs(extract_shelf_from_waypoint(wp_source_down, is_vertex=True) - extract_shelf_from_location(cur_location)) \
                         + abs(extract_shelf_from_waypoint(wp_target_down, is_vertex=True) - extract_shelf_from_location(next_location))

        self.report({'INFO'}, "UP COUNT: " + str(up_shelf_count))
        self.report({'INFO'}, "DOWN COUNT: " + str(down_shelf_count))
        
        if last_direction == 'asc':
            return down_shelf_count > up_shelf_count
        else:
            return up_shelf_count > down_shelf_count
    
    #=======================================================================================================
    base_location = [ob for ob in bpy.data.objects if re.search("Base", ob.name)][0].data.vertices[7]
    locations = resort_locations(locations, 'asc')
    last_direction = 'asc'
    
    way = []
    way.append(base_location)
    visited_location = append_way(locations[0], base_location, last_direction, 'asc')
    
    
    # sorting the locations according aisles and shelf_number
    #if we at the top of aisle we have to reverse the list of the next aisle. so we go from the top to dowm.
    desc = False
    switched = False
    while len(locations) > 0:
        next_location = locations[0]
        next_aisle = extract_aisle_from_location(next_location)
        visited_aisle = extract_aisle_from_location(visited_location)
        next_shelf = extract_shelf_from_location(next_location)
        visited_shelf = extract_shelf_from_location(visited_location)
        if visited_aisle == next_aisle:
            visited_location = append_way(next_location)
            last_direction= 'desc' if desc else 'asc'
            switched = False
        elif (is_same_area(visited_location, next_location) or (desc and next_shelf > visited_shelf) or (not desc and next_shelf < visited_shelf)) and not switched and way_strategy == 'snake':
            # resort list arcording the snake algorithm
            locations = resort_locations(locations, 'asc' if desc else 'desc')
            desc = not desc
            switched = True
        elif (way_strategy == 'shortest_way' and change_direction(visited_location, next_location, last_direction) and not switched):
            # resort list 
            locations = resort_locations(locations, 'asc' if desc else 'desc')
            desc = not desc
            switched = True
        else:  
            # add waypoints as we have to change the aisles
            visited_location = append_way(next_location, visited_location, last_direction, 'desc' if desc else 'asc', switch_strategy=way_strategy)
            last_direction= 'desc' if desc else 'asc'
            switched = False

    way.append(get_next_waypoints(visited_location, base_location, last_direction, 'desc')[0])
    way.append(base_location)
    
    # draw way
    if draw_routes:
        verts = [ v.co for v in way if v.index not in (32, 33 ,34, 35)]
        edges = [[i, i+1] for i in range(len(verts)-1)]
    
        [add_visted_location(self, context, v.co, get_location_name(v, is_pick_point=False)) for v in way if v.index in (32, 33 ,34, 35)] 

        mesh = bpy.data.meshes.new(name="Route")
        mesh.from_pydata(verts, edges, faces=[])
        # useful for development when the mesh may be invalid.
        # mesh.validate(verbose=True)
        object_data_add(context, mesh, operator=self)
    
    return way          

"""
function to extract the location_name from the object
to get the level of the location we have to substract 31 from the index as the indeces for the locations of the shelf are at the position 36 to 39.
"""
def get_location_name(location_object, is_pick_point=True):
    return location_object.id_data.name.split(' ')[1].split(".")[0] + "." + str(location_object.index - (35 if is_pick_point else 31)) if re.search("Shelf", location_object.id_data.name) else location_object.id_data.name.rsplit('.', 1)[0]


def is_duplicate(location, locations):
    for loc in locations:
        if loc.id_data.name == location.id_data.name and loc.index == location.index:
            return True
    return False

def get_random_location():
    shelf = random.choice(SHELFS)
    return random.choice(shelf.data.vertices[32:36]) # only these incides holds the locations


"""
get location coordinates and store to file
"""
locations = []
def create_init_configuration(self, context):
    for shelf in SHELFS:
        for loc in range(32,36):
            l = shelf.data.vertices[loc]
            locations.append([get_location_name(l, is_pick_point=False), l.co[0], l.co[1], l.co[2]])

    base = [ob for ob in bpy.data.objects if re.search("Base", ob.name)][0].data.vertices[7]
    locations.append(["Base", base.co[0], base.co[1], base.co[2]])
    df = pd.DataFrame(locations, columns = ['location_no','co_x','co_y', 'co_z'])
    df.sort_values(by='location_no', inplace=True)
    df.drop(['location_no'], axis=1, inplace=True)
    self.report({'INFO'}, str(df))
    df.to_csv("gen_data/init_config.csv")
"""
Function to generate data from simulated picks
number_of_picks: generate data from the given amount of picks
draw_routes: draw also the route of the picks in the warehouse area
general info: index 26-39 represent the pick point, which is the location from the picker, the palce where as the picker tries to pick the item from
and we do not calculate the location itself into the distance. the location is just for visulation.
"""
def create_dataset(self, context, number_of_picks, way_strategy='snake', draw_routes=False, generate_output_file=True, gen_random=True):
    ## 
    # initiate empty array for pandas dataset
    loc_distances = np.empty((0,4), dtype='object') #  ## order_id, location_from, location_to, distance
    
    all_location = []
    if gen_random is False:
        for shelf in SHELFS:
            for loc in range(32,36):
                l = shelf.data.vertices[loc]
                all_location.append([l,'x'])
        # cur = c
        # visited = v
        # done = d
        all_location[0][1] = 'c' # set start location
        all_ways = []
        found_pair = False
        for i, l in enumerate(all_location):
            print(l)
            if l[1] == 'd':
                continue
            if l[1] == 'c':
                for j, ol in enumerate(all_location):
                    if ol[1] == 'x':
                        found_pair = True
                        all_location[j][1]='v'
                        all_ways.append([l[0], ol[0]])

                all_location[i][1] = 'd'

                if i == len(all_location) -3:
                    all_ways.append([all_location[i+1][0], all_location[i+2][0]])
                    break
                all_location[i+1][1] = 'c'
                for k, ool in enumerate(all_location):
                    if ool[1] == 'v':
                       all_location[k][1] = 'x'

        number_of_picks = len(all_ways)


    for pick_count in range(number_of_picks):
        locations = []
        #random.seed(255+pick_count) # used for generating the data
        if gen_random:
            for loc in range(random.randint(7,10)): # shuffle between 1 and 10 locations 
                location = get_random_location()
                
                while is_duplicate(location, locations):
                    location = get_random_location()

                locations.append(location)
        else:
            locations = all_ways[pick_count]
           
        picking_route = create_way(self, context, way_strategy, locations, draw_routes)

        # remove location itself from the picking route, it isn't needed for the calculation of the way
        [picking_route.remove(wp) for wp in picking_route if re.search("Shelf", wp.id_data.name) and wp.index in (32, 33, 34 ,35)]

        # get the number of shelfs in the route
        shape_size = len([wp for wp in picking_route if re.search("Shelf", wp.id_data.name)])
            
        loc_distance = np.empty((shape_size +1,4), dtype='object') ## order_id, location_from, location_to, distance
        
        last_wp = None
        last_shelf_location = picking_route[0]
        sl_idx = 0
        distance = 0
        for wp in picking_route:
            if last_wp is not None:
                distance += math.dist(last_wp.co, wp.co)
            
            if (re.search("Shelf", wp.id_data.name)) or (re.search("Base", wp.id_data.name) and last_wp is not None):
                loc_distance[sl_idx] = [pick_count, get_location_name(last_shelf_location), get_location_name(wp), distance]
                sl_idx += 1
                distance = 0
                last_shelf_location = wp                
            last_wp = wp
            
        loc_distances = np.vstack((loc_distances, loc_distance))
    df = pd.DataFrame(loc_distances, columns = ['id','source_location','target_location', 'distance'] )
    self.report({'INFO'}, str(df))
    if generate_output_file:
        df.to_feather("gen_data/simulated_picks.feather")
        df.to_csv("gen_data/simulated_picks.csv")
            

class OBJECT_OT_add_object(Operator, AddObjectHelper):
    """Create a new Mesh Object"""
    bl_idname = "mesh.add_object"
    bl_label = "Add Warehouse area"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        # define GLOBAL variables which won't be changed after creating the warehouse
        global SHELFS
        global WAYPOINTS

        """
        1 unit = 1000mm
        typical dimensions of a shelf = 1800x900x400mm
        typical aisle width according (Gud2012 S.733) is between 1000 and 1500 mmm
        the best height for locating items in a shelf is 1200 as also referenced in (Martin2016)
        """
        #define some variables
        x_right = 0
        x_left = 0
        aisles = 1
        aisle_deep =2
        shelfs_in_row = 2
        aisle_width = 1.2 # -> 1200mm
        shelf_deep = 0.4 # -> ~ 400mm
        shelf_width = 0.9 # -> ~ 900mm
        y = 0
        z = 1.2 # perfect pick height for a picker
        
        total_width = aisle_width + shelf_deep*2
        x_start = (aisles*total_width)/2
        add_base(self, context, [x_start,y, z])
        
        for a, a_name in zip(range(aisles), string.ascii_uppercase):
            x_left = a*(aisle_width + shelf_deep*2)
            x_right = x_left + shelf_deep + aisle_width
        
            y_aisle_offset = aisle_width
            for d in range(aisle_deep):
                y = d*shelf_width + y_aisle_offset

                if (d % shelfs_in_row == 0):
                    if d != 0:
                        y += aisle_width
                        y_aisle_offset += aisle_width

                    add_way_point(self,context,[x_left+shelf_deep+aisle_width/2, y-aisle_width/2, z], "Waypoint " + a_name + str(d*2+1).zfill(3))

                    
                shelf_name = "Shelf " + a_name          
                shelf_number_count = d*2+2
                even_shelf_number = shelf_number_count if shelf_number_count % 2 == 0 else shelf_number_count +1
                odd_shelf_number = shelf_number_count-1 if shelf_number_count % 2 == 0 else shelf_number_count

                add_shelf(self, context, [x_left, y], shelf_deep, (shelf_name + str(even_shelf_number).zfill(3)), aisle_width,z, orientation="l")
                add_shelf(self, context, [x_right, y], shelf_deep, (shelf_name + str(odd_shelf_number).zfill(3)), aisle_width,z, orientation="r")

            add_way_point(self,context,[x_left+shelf_deep+aisle_width/2, y+shelf_width+aisle_width/2, z], "Waypoint " + a_name + str(aisle_deep*2+1).zfill(3))

        SHELFS = [ob for ob in bpy.data.objects if re.search("Shelf", ob.name)]
        WAYPOINTS = [ob for ob in bpy.data.objects if re.search("Waypoint", ob.name)]
        
        # possible strategies: snake, return_adapted, shortest_way
        create_init_configuration(self, context)
        create_dataset(self,context, number_of_picks=1, way_strategy='return_adapted', draw_routes=False, generate_output_file=True, gen_random=False)
        return {'FINISHED'}


# Registration

def add_object_button(self, context):
    self.layout.operator(
        OBJECT_OT_add_object.bl_idname,
        text="Add Warehouse area",
        icon='PLUGIN')

def register():
    bpy.utils.register_class(OBJECT_OT_add_object)
    bpy.types.VIEW3D_MT_mesh_add.append(add_object_button)
    
def unregister():
    bpy.utils.unregister_class(OBJECT_OT_add_object)
    bpy.types.VIEW3D_MT_mesh_add.remove(add_object_button)
    

if __name__ == "__main__":

    register()
